import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { loadPosts, removePost, addPost } from './store/actions';
import { fold } from './remote';

function App() {
  const [value, setValue] = useState('');
  const [text, setText] = useState('');
  const posts = useSelector((state) => state.posts);
  console.log(posts);
  const { addLoading, removeLoading } = useSelector((state) => state.loadings);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadPosts());
  }, []);

  function removePostById(id) {
    if(!removeLoading) {
      dispatch(removePost(id));
    }
  }

  function addTask(e) {
    e.preventDefault();
    dispatch(addPost(value, text));
  }

  const viewPosts = fold(
    () => 'Пусто...',
    () => 'Loading...',
    (data) => data.map((post) => (
      <Post
        post={post} key={post.id}
        removePost={removePostById}
      />
    )),
    () => 'Произошла ошибка...'
);

  return (
    <div>
      <form onSubmit={addTask}>
        <input
          type="text"
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
        <textarea
          value={text}
          onChange={(e) => setText(e.target.value)}
        ></textarea>
        <button disabled={addLoading}>Добавить</button>
      </form>
      {viewPosts(posts)}
    </div>
  )
}

function Post({ post, removePost }) {
  return (
    <article onClick={() => removePost(post.id)}>
      <h1>{post.title}</h1>
      <p>{post.body}</p>
      <hr/>
    </article>
  )
}

export default App;
