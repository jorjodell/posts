import { actionTypes } from './actions';
import { combineReducers } from 'redux';
import { initialize, pending, success } from '../remote';


function posts(state = initialize(), action) {
	switch(action.type) {
		case actionTypes.LOAD_POSTS: {
			return pending();
		}
		case actionTypes.LOAD_POSTS_SUCCESS: {
			return success(action.posts);
		}
		default: return state;
	}
}
const initialLoadingsState = {
	loadLoading: false,
	addLoading: false,
	removeLoading: false,
}

function loadings(state = initialLoadingsState, action) {
	switch(action.type) {
		case actionTypes.CHANGE_LOADING:
			return {
				...state,
				[`${action.name}Loading`]: action.value
			}
		default: return state;
	}
}

const state = {
	posts,
	loadings
}

export default combineReducers(state);