import { put, takeEvery, select } from 'redux-saga/effects';
import {
  actionTypes,
  loadPostsSuccess,
  removePostSuccess,
  addPostSuccess,
  changeLoading
} from './actions';
import { addPostApi, getPostsApi, removePostApi } from '../api';

function* loadPostsSaga() {
  try {
    const res = yield getPostsApi();
    const data = yield res.json();
    yield put(loadPostsSuccess(data));
  } catch (error) {
    console.error(error);
  }
  
}

function* addPostSaga(action) {
  const { title, body } = action;
  yield put(changeLoading('add', true));
  const res = yield addPostApi(title, body);
  const data = yield res.json();
  yield put(changeLoading('add', false));

  yield put(addPostSuccess({
    id: data.id,
    title,
    body
  }))
}

function* removePostSaga(action) {
  const posts = yield select((state) => state.posts);
  const filteredPosts = posts.data.filter(
    (post) => post.id !== action.postId
  );
  yield removePostApi(action.postId);
  yield put(loadPostsSuccess(filteredPosts));
}

export default function* rootSaga() {
  yield takeEvery(actionTypes.LOAD_POSTS, loadPostsSaga);
  yield takeEvery(actionTypes.REMOVE_POST, removePostSaga);
  yield takeEvery(actionTypes.ADD_POST, addPostSaga);
}