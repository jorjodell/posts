export const actionTypes = {
  LOAD_POSTS: 'LOAD_POSTS',
  LOAD_POSTS_SUCCESS: 'LOAD_POSTS_SUCCESS',

  REMOVE_POST: 'REMOVE_POST',
  REMOVE_POST_SUCCESS: 'REMOVE_POST_SUCCESS',

  ADD_POST: 'ADD_POST',
  ADD_POST_SUCCESS: 'ADD_POST_SUCCESS',

  CHANGE_LOADING: 'CHANGE_LOADING'
}

export const loadPosts = () => ({type: actionTypes.LOAD_POSTS});
export const loadPostsSuccess = (posts) => ({
  type: actionTypes.LOAD_POSTS_SUCCESS,
  posts
})

export const removePost = (postId) => ({
  type: actionTypes.REMOVE_POST,
  postId
})
export const removePostSuccess = (postId) => ({
  type: actionTypes.REMOVE_POST_SUCCESS,
  postId
})


export const addPost = (title, body) => ({
  type: actionTypes.ADD_POST,
  title,
  body
})

export const addPostSuccess = (post) => ({
  type: actionTypes.ADD_POST_SUCCESS,
  post
})

export const changeLoading = (name, value) => ({
  type: actionTypes.CHANGE_LOADING,
  name,
  value
})