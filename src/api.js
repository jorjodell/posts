export function getPostsApi() {
  return fetch('https://jsonplaceholder.typicode.com/posts');
}

export function addPostApi(title, body) {
  return fetch(
    'https://jsonplaceholder.typicode.com/posts',
    {
      method: 'POST',
      body: JSON.stringify({
        title,
        body,
        userId: 1
      }),
    }
  );
}

export function removePostApi(postId) {
  return fetch(
    `https://jsonplaceholder.typicode.com/posts/${postId}`,
    { method: 'DELETE' }
  );
}