export const stateStatus = {
	INITIAL: 'INITIAL',
	PENDING: 'PENDING',
	SUCCESS: 'SUCCESS',
	FAILURE: 'FAILURE',
}

export const initialize = () => ({
  status: stateStatus.INITIAL
})
export const pending = () => ({
  status: stateStatus.PENDING
})
export const success = (data) => ({
  status: stateStatus.SUCCESS, data
})
export const failure = (error) => ({
  status: stateStatus.FAILURE, error
})

export function fold(
  cbInitialize,
  cbPending,
  cbSuccess,
  cbFailure
) {
  return (state) => {
    switch(state.status) {
      case stateStatus.INITIAL: return cbInitialize();
      case stateStatus.PENDING: return cbPending();
      case stateStatus.SUCCESS: return cbSuccess(state.data);
      case stateStatus.FAILURE: return cbFailure();
      default: throw Error('Отсутствует статус...');
    }
  }
}

