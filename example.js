// class createStore {
//   constructor(reducer, initialState = null) {
//     this._reducer = reducer;
//     this._subsribers = [];
//     this.state = initialState;
//   }
//   distapch(action) {
//     this.state = this._reducer(this.state, action);
//     this.deliver();
//   }
//   getState() {
//     return this.state;
//   }
//   subscribe(method, context = null) {
//     this._subsribers.push({method, context});
//   }
//   unsubscribe(method, context = null) {
//     this._subsribers = this._subsribers.filter((el, id) => {
//       return method !== el.method
//     })
//   }
//   deliver() {
//     this._subsribers.forEach(subsriber => {
//       subsriber.method.call(subsriber.context, this.state);
//     })
//   }
// }


// function appReducer(state, action) {
//   switch(action.type) {
//     case 'ADD_TODO': return [...state, action.payload];
//     default: return state;
//   }
// }

// const store = new createStore(appReducer, [
//   'Coding',
//   'Breakfast',
//   'Listen a music'
// ]);

// store.distapch({
//   type: 'ADD_TODO',
//   payload: 'Book reading'
// });

// const logState = () => console.log(store.getState());

// store.subscribe(logState);

// store.distapch({
//   type: 'ADD_TODO',
//   payload: 'Pizza time!'
// })

// // store.unsubscribe(logState)

// store.distapch({
//   type: 'ADD_TODO',
//   payload: 'And again coding!'
// })

const initialState = ['world', 'hello', 'abc', '1b'];

function myReducer(state, action) {
  switch(action.type) {
    case 'REMOVE_TEXT': return state.filter(
      (el, i) => i !== action.index
    )
    case 'ADD_TEXT': return [...state, action.text]
    case 'GET_SHORT_TEXT': return state.filter((txt) => txt.length < 4)
    default: return state;
  }
}

function dispatch(action) {
  myReducer(this.state, action);
}

const getShortText = () => ({type: 'GET_SHORT_TEXT'});
const addText = (txt) => ({type: 'ADD_TEXT', text: txt})

dispatch(addText('New wadfasdf!'));